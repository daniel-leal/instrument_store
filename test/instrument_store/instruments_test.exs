defmodule InstrumentStore.InstrumentsTest do
  use InstrumentStore.DataCase

  alias InstrumentStore.Instruments

  describe "guitars" do
    alias InstrumentStore.Instruments.Guitar

    import InstrumentStore.InstrumentsFixtures

    @invalid_attrs %{is_available: nil, model: nil, year: nil}

    test "list_guitars/0 returns all guitars" do
      guitar = guitar_fixture()
      assert Instruments.list_guitars() == [guitar]
    end

    test "get_guitar!/1 returns the guitar with given id" do
      guitar = guitar_fixture()
      assert Instruments.get_guitar!(guitar.id) == guitar
    end

    test "create_guitar/1 with valid data creates a guitar" do
      valid_attrs = %{is_available: true, model: "some model", year: 42}

      assert {:ok, %Guitar{} = guitar} = Instruments.create_guitar(valid_attrs)
      assert guitar.is_available == true
      assert guitar.model == "some model"
      assert guitar.year == 42
    end

    test "create_guitar/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Instruments.create_guitar(@invalid_attrs)
    end

    test "update_guitar/2 with valid data updates the guitar" do
      guitar = guitar_fixture()
      update_attrs = %{is_available: false, model: "some updated model", year: 43}

      assert {:ok, %Guitar{} = guitar} = Instruments.update_guitar(guitar, update_attrs)
      assert guitar.is_available == false
      assert guitar.model == "some updated model"
      assert guitar.year == 43
    end

    test "update_guitar/2 with invalid data returns error changeset" do
      guitar = guitar_fixture()
      assert {:error, %Ecto.Changeset{}} = Instruments.update_guitar(guitar, @invalid_attrs)
      assert guitar == Instruments.get_guitar!(guitar.id)
    end

    test "delete_guitar/1 deletes the guitar" do
      guitar = guitar_fixture()
      assert {:ok, %Guitar{}} = Instruments.delete_guitar(guitar)
      assert_raise Ecto.NoResultsError, fn -> Instruments.get_guitar!(guitar.id) end
    end

    test "change_guitar/1 returns a guitar changeset" do
      guitar = guitar_fixture()
      assert %Ecto.Changeset{} = Instruments.change_guitar(guitar)
    end
  end
end
